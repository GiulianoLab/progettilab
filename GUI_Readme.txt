Interfaccia grafica per il corso di SESM:

- funzione di visulaizzazione che prende come parametro un puntatre ad un array e la size;
- il numero dei valori puo' essere maggiore rispetto alla dimensione in pixel, 
servira' per l'analizzatore di spettro dove sara' possibile passare 1024 punti o 2048 o 4096 in base alla implementazione e 
tramite gesture DRAG sara' possibile scorrere la finestra di visualizzazione.
- il valore di questi dati sara' in digit, il chart fara' automaticamente la scaltura rispetto al massimo dell'array;
- La scala verticale sara' gestita automaticamente, il grafico visualizzera' in corrsipondenza del massimo e del minimo un valore numerico (per il momento il minimo sara' fisso 0, il massimo corrisponde al massimo dell'array, ad es. 1000 --> 1,000 V).
- La scala orizzontale verra' gestita tramite fattore di scala, che fara' corrispondere ai pixel il tempo o la frequenza. 
- L'interfaccia gestisce il singolo tocco, al quale fa corrispondere una callback con le coordinate x e y del tocco. A queste coordinate l'oscilloscpio fara' corrispondere posizione e livello di trigger, l'analizzatore di spettro potrebbe usarlo per altro, ad esempio per ruotare il tipo di finestra adottata.
- L'interfaccia deve gestire pinch e zoom, serviranno per una callback che aumenta o riduce lo zoom orizzontale. Nel caso dell'oscilloscopio l'untente cambiera' opportunamente la frequenza di campionamento per passare ai secondi a divisione succesivi, con salti discreti del tipo 1, 2, 5, 10. Nel caso dell'analizzatore di spettro l'utente valutera' se spostare N (1024, 2048 o 4096) e/o cambiare la fequenza di campionamento, per ridurre o aumetare la risoluzione in frequenza, aumentando o riducendo il tempo di analisi. In entrambi i casi l'utente dovra' aggiorare il fattore di scala verticale coerentemente.
Nel caso della FFT sui 1024 (o 2048 o 4096) valori, in posizione 0 ci sara' sempre la DC, in posizione 1024 (o 2048 o 4096) ci sara' una frequnza pari a fc/2.
